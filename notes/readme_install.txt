########### mysql dual db jpa
CREATE DATABASE kew2db DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE DATABASE kew3db DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE USER 'kewtea'@'localhost' IDENTIFIED BY '3889';
GRANT ALL ON kew2db.* TO 'kewtea'@'localhost';
GRANT ALL ON kew3db.* TO 'kewtea'@'localhost';
FLUSH PRIVILEGES;


grant all on kew2db.* to 'kewtea'@'localhost' identified by '3889';

SHOW GRANTS FOR 'root'@'localhost';
SHOW GRANTS
###########
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install
brew update
brew install elasticsearch

brew tap homebrew/services
brew services start mysql
brew services start elasticsearch
brew services list
brew services cleanup

curl -XGET 'localhost:9200/?pretty'
curl -XGET 'localhost:9200/customer/external/1?pretty'
curl -XGET 'localhost:9200/sysrecord-1702/sysrecord/1?pretty'
curl -XGET 'localhost:9200/sysrecord-*/sysrecord/_search?pretty'
curl -XGET 'http://localhost:9200/_cat/indices?v'
curl -XGET 'http://localhost:9200/_mapping?pretty=true'

curl -XGET http://localhost:9200/log-2012-12-27/_status 
curl -XGET 'http://localhost:9200/log-2012-12-27/_search?q=host:host2'
curl -XGET 'http://localhost:9200/log-2012-12-27/hadoop/_mapping'

config/elasticsearch.yml => cluster.name: es-cluster

######### setting 2.x
/etc/elasticsearch/elasticsearch.yml, {path.home}/config, {path.home}/data
home: /usr/share/elasticsearch
data: /var/lib/elasticsearch/data

ES=$HOME/workspace/elasticsearch/elasticsearch-1.6.0
$ES/bin/elasticsearch -Des.pidfile=$ES/bin/es.pid -Des.config=$ES_NODE/config/elasticsearch.yml -Djava.net.preferIPv4Stack=true -Des.max-open-files=true > /dev/null 2>&1 &
######### setting 5.x
$ES_HOME/config/, /etc/elasticsearch/
/etc/default/elasticsearch

brew osx: /usr/local/bin/elasticsearch
brew info elasticsearch
/usr/local/etc/elasticsearch/elasticsearch.yml
readlink after which elasticsearch
/usr/local/Cellar/elasticsearch/5.2.0/libexec/config
path.data: /usr/local/var/elasticsearch/
path.home:
path:
    data: /var/lib/elasticsearch
    logs: /var/log/elasticsearch
###############
http://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-nosql.html#boot-features-elasticsearch
https://dzone.com/articles/first-step-spring-boot-and
http://stackoverflow.com/questions/32837507/spring-boot-elasticsearch-configuration
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artificatId>spring-boot-starter-data-elasticsearch</artificatId>
</dependency>

application.properties
spring.data.elasticsearch.properties.path.home=/foo/bar
spring.data.elasticsearch.cluster-nodes=localhost:9300
spring.data.elasticsearch.cluster-nodes=10.10.1.10:9300

@Component
public class MyBean {

    private ElasticsearchTemplate template;

    @Autowired
    public MyBean(ElasticsearchTemplate template) {
        this.template = template;
    }

    // ...

}


@Configuration
public class ElasticSearchConfiguration {
    @Value("${spring.data.elasticsearch.cluster-name}")
    private String clusterName;
    @Value("${spring.data.elasticsearch.cluster-nodes}")
    private String clusterNodes;
    @Bean
    public ElasticsearchTemplate elasticsearchTemplate() throws UnknownHostException {
            String server = clusterNodes.split(":")[0];
            Integer port = Integer.parseInt(clusterNodes.split(":")[1]);
            Settings settings = Settings.settingsBuilder()
                .put("cluster.name", clusterName).build();
            client = TransportClient.builder().settings(settings).build()
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(server), port));
            return new ElasticsearchTemplate(client);

}

@Configuration
@PropertySource(value = "classpath:config/elasticsearch.properties")
public class ElasticsearchConfiguration {

    @Resource
    private Environment environment;

    @Bean
    public Client client() {
        TransportClient client = new TransportClient();
        TransportAddress address = new InetSocketTransportAddress(
                environment.getProperty("elasticsearch.host"), 
                Integer.parseInt(environment.getProperty("elasticsearch.port"))
        );
        client.addTransportAddress(address);        
        return client;
    }

    @Bean
    public ElasticsearchOperations elasticsearchTemplate() {
        return new ElasticsearchTemplate(client());
    }
}

######### sysrecord(index), log-1701(type), filebeat-2017.02.16
curl -XPUT 'localhost:9200/sysrecord?pretty' -H 'Content-Type: application/json' -d'
{
    "settings" : {
        "index" : {
            "number_of_shards" : 3, 
            "number_of_replicas" : 2 
        }
    }
}
'

curl -XPUT 'http://localhost:9200/blog/user/dilbert' -d '{ "name" : "Dilbert Brown" }'
curl -XPOST 'http://localhost:9200/blog/post' -d '
curl -XPUT 'http://localhost:9200/blog/post/1' -d '
{ 
    "user": "dilbert", 
    "postDate": "2011-12-15", 
    "body": "Search is hard. Search should be easy." ,
    "title": "On search"
}'
curl -XGET 'http://localhost:9200/blog/user/dilbert?pretty=true'
curl -XGET 'http://localhost:9200/blog/post/1?pretty=true'
curl 'http://localhost:9200/blog/post/_search?q=user:dilbert&pretty=true'

curl -XPUT 'http://localhost:9200/log//dilbert' -d '{ "name" : "Dilbert Brown" }'

curl -XGET 'http://localhost:9200/filebeat-*/_search?pretty'

