package com.dmalt.repositoryelastic;

import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.dmalt.modelelastic.SysRecord;

public interface SysRecordRepository extends ElasticsearchRepository<SysRecord, String> {
	
	public List<SysRecord> findByTitle(String title);
	/*
	 Page<Article> findByAuthorsName(String name, Pageable pageable);
 
    @Query("{\"bool\": {\"must\": [{\"match\": {\"authors.name\": \"?0\"}}]}}")
    Page<Article> findByAuthorsNameUsingCustomQuery(String name, Pageable pageable);

	 
	 
	 */
	
	//List<SysRecord> findBy....
}
