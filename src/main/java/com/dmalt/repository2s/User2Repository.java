package com.dmalt.repository2s;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dmalt.model2s.User2;

@Repository
public interface User2Repository extends JpaRepository<User2, Long> {

}
