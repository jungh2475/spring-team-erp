package com.dmalt;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.dmalt.model2s.User2;
import com.dmalt.modelelastic.SysRecord;
import com.dmalt.models.ContentOld;
import com.dmalt.models.User;
import com.dmalt.repositories.ContentRepository;
import com.dmalt.repositories.UserRepository;
import com.dmalt.repository2s.User2Repository;
import com.dmalt.repositoryelastic.SysRecordRepository;

//사이트가 처음 스타트할때 리스너로 연락받아서 데이터 베이스에 값을 적어 놓는다. 개발중에 편리함. 나중에 application.properties에서 db 자동 삭제를 꺼주여야 함 
@Component
public class DatabaseLoader implements ApplicationListener<ContextRefreshedEvent> {
	
	static Logger log = Logger.getLogger(DatabaseLoader.class.getName());
	
	@Autowired
	private UserRepository userRepo;
	@Autowired
	private User2Repository user2Repo;
	@Autowired
	private ContentRepository contentRepo;
	//@Autowired
	//private SysRecordRepository sysrecordRepo;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		try {
			
			log.info("creating db record content...");
			
			User user = new User();
			user.email="abc@email.com";
			user.password="abcdef";
			user.name="ssserf";
			user.uniquename="dkjhs";
			userRepo.save(user);
			
			User2 user2 = new User2();
			user2.comment="dodjhfskjsdkjfhs";
			user2.email="bbc@email.com";
			user2.password="woehfwoefh";
			user2.name="akfhskdjfh";
			user2.uniquename="wehowfhoi";
			user2Repo.save(user2);
			
			
			ContentOld content = new ContentOld();
			content.body="hihi";
			content.comments="gggggg";
			contentRepo.save(content);
			
			/*
			log.info("creating elastic record content...");
			SysRecord sysRecord = new SysRecord();
			sysRecord.id="5";
			sysRecord.title="sample5";
			sysrecordRepo.save(sysRecord);
			
			log.info("saved as "+ sysrecordRepo.findByTitle(sysRecord.title).get(0).toString());
			//delete(ID id) 
			 
			*/
			log.info("dbLoader saved");
			
		} catch (Exception e){
			log.info("dbLoader failed");
			e.printStackTrace();
			
		}
	}

}
