package com.dmalt.services;

import java.util.logging.Logger;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;




/*
 *  1) Copy Repository1(newer ones) -> Repo2 (User, Content, Tags) //이렇게 해서 점진적으로 Repo2만 사용 
 *	2) Move Repo 1(older ones) -> Repo 2 & Delete some of Repo 1 //새로운 자료만 Repo1에 남김 
 *  3) User Profile 만들기, User Tag Snapshot 만들기 (words of day/weeks/months)
 *  4) Log cleaning (size management) -> 일정 시간 이전 것 지우기  
 *  5) Db cleansing (user, content, tags)
 *  6) User info transfer : wordpress에 저장된 사용자를 main DB에 등록 하기 
 *  7) mailing list (단체 메일 발송 ) 
 */

@Service
public class BatchService {
	
	static Logger log = Logger.getLogger(BatchService.class.getName());
	
	@Autowired
	JobLauncher jobLauncher;
	@Autowired
	Job job;
	
	public void runBatch1() throws Exception {
		jobLauncher.run(job, new JobParameters());
		log.info("batch1 is executed not completed as async");
	}
	

}
