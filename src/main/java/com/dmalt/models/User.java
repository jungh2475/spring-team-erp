package com.dmalt.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor  //@NoArgsConstructor
@Table(name = "users")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	
	@NotEmpty 
	@Email
	@Column(unique=true, nullable=false)
	public String email;	
	
	//@JsonIgnore  //Rest에서 패스워드 노출되는 것을 막을려면, 이렇게 한다 .., EncodedPassword면 60char이상 값이 저장될것임 
	@Size(min = 4)
	public String password;
	
	@Column(nullable = true)
	public String name;  //John Smith
	
	@Column(unique=true, nullable=true)
	public String uniquename;   // url-mapped
	
	public User() {}
	public User(User user) {}
	
}
