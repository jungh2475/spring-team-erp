package com.dmalt.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Data  //@Getter @Setter
@AllArgsConstructor  //@NoArgsConstructor
@Table(name = "contentselastic")
public class ElasticContent extends Content {
	
	/*
	 * 
	 부모로부터 물려받은 매핑 정보를 재정의 : http://yellowh.tistory.com/123
	 		@AttributeOverrides({
		        @AttributeOverride(name = "id", column = @Column(name = "MEMBER_ID")),
		        @AttributeOverride(name = "name", column = @Column(name = "MEMBER_NAME"))
			})

	*/	
		//추가 되는 항목들 
	
	@Column(nullable = true)
	public String contenttype;	//order(quote,order), report, chart, task(tasktemplate, task, action), 
	//webhook (slack), script(js, python), 
	@Lob
	@Column(columnDefinition = "TEXT")
	public String hgroup;
	@Lob
	@Column(columnDefinition = "TEXT")
	public String body;
	
	
	
}
