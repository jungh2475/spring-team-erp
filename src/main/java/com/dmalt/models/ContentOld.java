package com.dmalt.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//product, view, report, task(of erp), activity(of organisation), organisation-group, data-adapter 

@Entity
@Data
@AllArgsConstructor  
@NoArgsConstructor
@Table(name = "contents")
public class ContentOld {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	
	@Lob
	@Column(columnDefinition = "TEXT")
	public String hgroup;
	@Lob
	@Column(columnDefinition = "TEXT")
	public String body;     //이 안에다가 comment_id들을 넣으세요. 나중에는 fb/gCanlendar/날씨등의 데이터도 같이 넣을수 있죠 
	
	@Column(nullable = true)
	public String comments; 

}
