package com.dmalt.adapters;

import java.util.List;

public interface DataConnectAdapter {
	
	public void config(String option); //전체 구성 파일 
	public List<String> connectGet(String account, String url); //정보 가져오기 
	public List<Long> connectSet(String account, String url);  //정보 저장 

}
