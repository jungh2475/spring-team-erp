package com.dmalt.batches;

import java.util.logging.Logger;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.boot.autoconfigure.batch.BatchProperties.Job;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



//https://spring.io/guides/gs/batch-processing/

@EnableAutoConfiguration
@Configuration
@EnableBatchProcessing
public class Job1Configuration {
	
	static Logger log = Logger.getLogger(Job1Configuration.class.getName());
	
	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Bean
	public Step step1() {
		
		log.info("job1-step1 is launched!");
		
	    return stepBuilderFactory.get("step1")
	        .tasklet(new Tasklet() {
	          public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) {
	            return null;
	          }
	        }) //.<Person, Person> chunk(10).reader(reader()).processor(processor()).writer(writer())
	        .build();   //chunck(10) ...10개씩 읽어서 한번에 처리 
	}

	@Bean
	public Job job(Step step1) throws Exception {
		
		log.info("job1 batch is launched!");
		
	    return jobBuilderFactory.get("job1")
	        .incrementer(new RunIdIncrementer())
	        .start(step1)  //.listener(listener).flow(step1()).end()
	        .build();
	}
}
