package com.dmalt.modelelastic;


import javax.persistence.Id;

import org.springframework.data.elasticsearch.annotations.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Document(indexName = "sysrecord-1702", type = "sysrecord", shards = 1, replicas = 0, refreshInterval = "-1")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysRecord {
 
    @Id
    public String id;
     
    public String title;

	@Override
	public String toString() {
		String returnStr="id,title:";
		returnStr=returnStr+id+","+title;
		return returnStr;
		//return super.toString();
	}
     
    //@Field(type = FieldType.Nested)
    //private List<Author> authors;
     
    // standard getters and setters
    
}
