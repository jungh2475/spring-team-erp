package com.dmalt.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dmalt.models.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

}
