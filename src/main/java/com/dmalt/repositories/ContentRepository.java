package com.dmalt.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dmalt.models.ContentOld;

@Repository
public interface ContentRepository extends JpaRepository<ContentOld, Long> {

}
