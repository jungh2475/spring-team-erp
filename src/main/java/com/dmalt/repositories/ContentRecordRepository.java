package com.dmalt.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dmalt.models.UserRecord;

@Repository
public interface ContentRecordRepository extends JpaRepository<UserRecord, Long> {
	
}
