package com.dmalt;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

import com.dmalt.batches.Job1Configuration;


@SpringBootApplication  //(exclude = HibernateJpaAutoConfiguration.class )이것 동작 안함 
public class ErpApplication implements CommandLineRunner {
	
	static Logger log = Logger.getLogger(ErpApplication.class.getName());
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(ErpApplication.class, args);
	}
	
	//@Autowired DataInitializer initializer;

	@PostConstruct
	public void init() {
		log.info("main application is loaded");
		//
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		if(args.length>0) log.info("running commands-nothing now with "+args[0]);
	}
}

/*
 	시스템이 꺼리면 스프링도 꺼진다. 대신에 처음 시작할때 Job1Configuration을 실행하라 
 	System.exit(SpringApplication.exit(SpringApplication.run(
				Job1Configuration.class, args)));
	ConfigurableApplicationContext ctx = SpringApplication.run(Application.class, args);
        System.exit(SpringApplication.exit(ctx));
  
*/