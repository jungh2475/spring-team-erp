package com.dmalt;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.dmalt.models.User;
import com.dmalt.services.UserService;

/*
 *  json test :
 	private JacksonTester<VehicleDetails> json;

    @Before
    public void setup() {
        ObjectMapper objectMapper = new ObjectMapper(); 
        // Possibly configure the mapper
        JacksonTester.initFields(this, objectMapper);
    }
    @Test
    public void deserializeJson() {
        String content = "{\"make\":\"Ford\",\"model\":\"Focus\"}";

        assertThat(this.json.parse(content))
            .isEqualTo(new VehicleDetails("Ford", "Focus"));

        assertThat(this.json.parseObject(content).getMake())
            .isEqualTo("Ford");
    }
 */

@RunWith(SpringRunner.class)
@SpringBootTest  //(webEnvironment=WebEnvironment.RANDOM_PORT)
public class ErpApplicationTests {
	/*
	
	@Autowired
    private TestRestTemplate restTemplate;

    @MockBean
    public UserService userService;
    
    @Autowired
    private TestEntityManager entityManager;  // -> this.entityManager.persist(user)


    @Test
    public void userRestTemplateTest() throws Exception {
        //User user = new User(1L, "wonwoo", "wonwoo@test.com", "123123");
        //given(this.userService.findOne(1L)).willReturn(user); //가짜 객체 주입 
        ResponseEntity<User> userEntity = this.restTemplate.getForEntity("/findOne/{id}", User.class, 1);
        User body = userEntity.getBody();
        //assertThat(body.name).isEqualTo("wonwoo");
        //assertThat(body.email).isEqualTo("wonwoo@test.com");
        ////assertThat(body.getPassword()).isEqualTo("123123");
        //assertThat(body.id).isEqualTo(1);
    }
    */
    
	@Test
	public void contextLoads() {
	}

}
